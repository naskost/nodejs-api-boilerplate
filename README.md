# Node.js API Boilerplate

This is a Node.js API boilerplate project using Express, Sequelize, MySQL, Redis, and Jest. It follows SOLID principles and includes authentication with JWT, password management, and a basic CI/CD pipeline with GitLab.

## Features

- Express for routing
- Sequelize for ORM
- MySQL for the database
- Redis for caching and session management
- Passport.js for authentication
- JWT for secure API access
- Jest for testing
- ESLint and Prettier for code quality
- GitLab CI/CD for continuous integration

## Prerequisites

- Node.js (v14 or later)
- MySQL
- Redis

## Getting Started

1. Clone the repository:
    ```bash
    git clone https://your-repo-url.git
    cd nodejs-api-boilerplate
    ```

2. Install dependencies:
    ```bash
    npm install
    ```

3. Set up your environment variables:
   Create a `.env` file based on the `.env.example` template:
    ```env
    DB_PROVIDER=mysql
    DB_NAME=my_database
    DB_USER=my_user
    DB_PASSWORD=my_password
    DB_HOST=localhost
    DB_PORT=3306
    REDIS_HOST=localhost
    REDIS_PORT=6379
    APP_PORT=3000
    SESSION_SECRET=your_session_secret_key
    JWT_SECRET=your_jwt_secret_key
    ```

4. Run migrations:
    ```bash
    npx sequelize-cli db:migrate
    ```

5. Start the application:
    ```bash
    npm start
    ```

## Running Tests

To run tests, use:

```bash
npm test