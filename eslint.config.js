module.exports = [
    {
        files: ['src/**/*.js'],
        languageOptions: {
            ecmaVersion: 2022, // Updated ECMAScript version representation to numeric
            sourceType: 'module',
        },
        ignores: ['**/*.config.js', '**/node_modules/**'], // Renamed "ignores" to "ignorePatterns"
        rules: {
            semi: [2, 'always'], // Made rule configuration more explicit
        },
    },
];