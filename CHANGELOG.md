# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- Initial project setup with Express, Sequelize, MySQL, Redis, Passport.js, and Jest.
- User authentication with JWT.
- Password change and expiration functionalities.
- Basic CI/CD pipeline with GitLab.

## [0.1.0] - 2024-05-25

### Added

- Initial release.