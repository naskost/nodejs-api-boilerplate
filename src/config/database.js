require('dotenv').config();
const { Sequelize } = require('sequelize');

const getDatabaseConfig = () => {
    switch (process.env.DB_PROVIDER) {
        case 'mysql':
            return {
                dialect: 'mysql',
                host: process.env.DB_HOST,
                port: process.env.DB_PORT,
                username: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
            };
        case 'postgres':
            return {
                dialect: 'postgres',
                host: process.env.DB_HOST,
                port: process.env.DB_PORT,
                username: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
            };
        case 'sqlite':
            return {
                dialect: 'sqlite',
                storage: process.env.DB_STORAGE || 'database.sqlite',
            };
        default:
            throw new Error('Unsupported DB_PROVIDER');
    }
};

const sequelize = new Sequelize(getDatabaseConfig());

module.exports = sequelize;