const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const bcrypt = require('bcryptjs');

const User = sequelize.define('User', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    passwordChangedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
    }
});

// Hash the password before saving the user
User.beforeCreate(async (user) => {
    user.password = await bcrypt.hash(user.password, 10);
});

// Update passwordChangedAt before updating the password
User.beforeUpdate(async (user) => {
    if (user.changed('password')) {
        user.passwordChangedAt = new Date();
    }
});

module.exports = User;