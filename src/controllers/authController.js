const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const { addTokenToBlacklist } = require('../utils/tokenBlacklist');
const { v4: uuidv4 } = require('uuid'); // Add uuid for generating unique token IDs

const generateToken = (user) => {
    const payload = {
        id: user.id,
        passwordChangedAt: user.passwordChangedAt,
        token: uuidv4(), // Generate a unique token ID
    };
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h' });
};

const changePassword = async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    const user = await User.findByPk(req.user.id);

    if (!user) {
        return res.status(404).json({ message: 'User not found' });
    }

    const isMatch = await bcrypt.compare(oldPassword, user.password);

    if (!isMatch) {
        return res.status(400).json({ message: 'Incorrect old password' });
    }

    user.password = await bcrypt.hash(newPassword, 10);
    user.passwordChangedAt = new Date();
    await user.save();

    res.json({ message: 'Password changed successfully' });
};

const logout = async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.decode(token);
    const expiry = decoded.exp - Math.floor(Date.now() / 1000); // Calculate the token's remaining time to live
    await addTokenToBlacklist(decoded.token, expiry);
    res.json({ message: 'Logged out successfully' });
};

module.exports = {
    generateToken,
    changePassword,
    logout,
};