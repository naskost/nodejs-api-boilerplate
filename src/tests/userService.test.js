const { User } = require('../models');
const userService = require('../services/userService');
const bcrypt = require('bcryptjs');

// Mock the User model
jest.mock('../models/User');

describe('UserService', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('createUser', () => {
        it('should create a new user with hashed password', async () => {
            const userData = {
                name: 'John Doe',
                email: 'john.doe@example.com',
                password: 'password123',
            };

            User.create.mockResolvedValue({
                id: 1,
                name: userData.name,
                email: userData.email,
                password: await bcrypt.hash(userData.password, 10),
            });

            const user = await userService.createUser(userData);

            expect(user).toHaveProperty('id');
            expect(user).toHaveProperty('name', userData.name);
            expect(user).toHaveProperty('email', userData.email);
            expect(user.password).not.toBe(userData.password); // Ensure the password is hashed
        });
    });

    describe('getAllUsers', () => {
        it('should return all users', async () => {
            const users = [
                { id: 1, name: 'John Doe', email: 'john.doe@example.com' },
                { id: 2, name: 'Jane Doe', email: 'jane.doe@example.com' },
            ];

            User.findAll.mockResolvedValue(users);

            const result = await userService.getAllUsers();

            expect(result).toEqual(users);
        });
    });
});