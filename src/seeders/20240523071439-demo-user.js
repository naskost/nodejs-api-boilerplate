'use strict';
const bcrypt = require('bcryptjs');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const hashedPassword1 = await bcrypt.hash('password123', 10);
        const hashedPassword2 = await bcrypt.hash('password456', 10);

        await queryInterface.bulkInsert('Users', [
            {
                name: 'John Doe',
                email: 'john.doe@example.com',
                password: hashedPassword1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                name: 'Jane Doe',
                email: 'jane.doe@example.com',
                password: hashedPassword2,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ], {});
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('Users', null, {});
    }
};