const express = require('express');
const passport = require('../config/passport');
const authController = require('../controllers/authController');
const { ensureAuthenticated } = require('../middleware/authMiddleware');

const router = express.Router();

// Login route
router.post('/login', (req, res, next) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: 'Something went wrong',
                user: user
            });
        }

        req.login(user, { session: false }, (err) => {
            if (err) {
                res.send(err);
            }

            // Generate a token
            const token = authController.generateToken(user);

            // Return the token and user info
            return res.json({ user, token });
        });
    })(req, res, next);
});

// Check if authenticated
router.get('/check-auth', ensureAuthenticated, (req, res) => {
    res.json({ authenticated: true, user: req.user });
});

// Change password route
router.post('/change-password', ensureAuthenticated, authController.changePassword);

// Logout route
router.post('/logout', ensureAuthenticated, authController.logout);

module.exports = router;