const express = require('express');
const userController = require('../controllers/userController');
const { ensureAuthenticated, checkPasswordExpiration } = require('../middleware/authMiddleware');

const router = express.Router();

router.get('/', ensureAuthenticated, checkPasswordExpiration, userController.getAllUsers);
router.post('/', userController.createUser);

module.exports = router;