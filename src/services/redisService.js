const Redis = require('ioredis');

class RedisService {
    constructor() {
        this.client = new Redis({
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
        });
    }

    async addTokenToBlacklist(token, expiry) {
        const key = `blacklist:${token}`;
        await this.client.set(key, 'blacklisted', 'EX', expiry);
    }

    async isTokenBlacklisted(token) {
        const key = `blacklist:${token}`;
        const result = await this.client.get(key);
        return result !== null;
    }
}

module.exports = new RedisService();