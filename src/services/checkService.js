require('dotenv').config();
const mysql = require('mysql2/promise');
const Redis = require('ioredis');

const checkMySQL = async () => {
    try {
        const connection = await mysql.createConnection({
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            port: process.env.DB_PORT,
        });
        await connection.execute('SELECT 1');
        console.log('MySQL is running');
        await connection.end();
    } catch (error) {
        console.error('MySQL is not running:', error.message);
        process.exit(1);
    }
};

const checkRedis = async () => {
    try {
        const redis = new Redis({
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
        });
        await redis.ping();
        console.log('Redis is running');
        redis.disconnect();
    } catch (error) {
        console.error('Redis is not running:', error.message);
        process.exit(1);
    }
};

const checkServices = async () => {
    await checkMySQL();
    await checkRedis();
    console.log('All services are running');
};

checkServices().then(r => r).catch(err => console.error(err));