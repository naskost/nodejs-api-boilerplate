const redisService = require('../services/redisService');

const addTokenToBlacklist = async (token, expiry) => {
    await redisService.addTokenToBlacklist(token, expiry);
};

const isTokenBlacklisted = async (token) => {
    return await redisService.isTokenBlacklisted(token);
};

module.exports = {
    addTokenToBlacklist,
    isTokenBlacklisted,
};
//
// const blacklist = new Set();
//
// const addTokenToBlacklist = (token) => {
//     blacklist.add(token);
// };
//
// const isTokenBlacklisted = (token) => {
//     return blacklist.has(token);
// };
//
// module.exports = {
//     addTokenToBlacklist,
//     isTokenBlacklisted,
// };