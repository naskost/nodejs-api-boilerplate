const passport = require('passport');

const ensureAuthenticated = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        return res.status(401).json({ message: 'Authorization header missing' });
    }

    const token = authHeader.split(' ')[1];

    passport.authenticate('jwt', { session: false }, (err, user, info) => {
        if (err) {
            return res.status(401).json({ message: 'Unauthorized', error: err });
        }

        if (!user) {
            return res.status(401).json({ message: 'Unauthorized', info });
        }

        req.user = user;
        next();
    })(req, res, next);
};

const checkPasswordExpiration = (req, res, next) => {
    const passwordExpiryDays = process.env.PASSWORD_EXPIRY_DAYS;
    const passwordChangedAt = new Date(req.user.passwordChangedAt);
    const currentDate = new Date();
    const diffDays = Math.floor((currentDate - passwordChangedAt) / (1000 * 60 * 60 * 24));

    if (diffDays > passwordExpiryDays) {
        return res.status(401).json({ message: 'Password expired, please change your password' });
    }

    next();
};

module.exports = {
    ensureAuthenticated,
    checkPasswordExpiration
};